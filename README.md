# Yeha Ghanta Milega (यहां घंटा मिलेगा)

This is a completely useless project, that's good for only laughs and giggles. Something to replace the drab 000-default index.html shipped with Apache2 on Debian.

## Getting Started

Simply git clone the repo. The base message with the bell is available in yehaghantamilega.xcf (think Gimp).

### Prerequisites

You may need a raster and a vector image / shape editing program to get the final changes made to the SVG file.

## Contributing

If you wish to contribute other funny place holder default index.html, feel free to send me a merge / pull request.

## Authors

* **Indranil Das Gupta** (https://gitlab.com/indradg/)

## License

This project is licensed under the CC-SA International 4.0 License - see the [LICENSE.md](LICENSE.md) file for details
